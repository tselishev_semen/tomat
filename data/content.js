tomato = {
    start: function() {
        tomato.processPage()
    },
    url: null,
    time: null,
    initialize: function(form){
      self.on('message', function(message){
          switch(message.type){
              case 'init':
                    tomato.url = message.url
                    tomato.time = message.time
                    tomato.createPanel(form)
                  break
          }
      })
    },
    processPage: function(){
        var inventory_form = document.getElementsByName('f1')[0]
        if(inventory_form && inventory_form.hasAttribute('action')){
            tomato.initialize(inventory_form)
        }
    },
    createTimeBox: function(parentObj){
        var input = document.createElement('input')
        input.setAttribute('type', 'text')
        input.setAttribute('value', tomato.time)
        input.innerHTML = tomato.time
        parentObj.appendChild(input)
        input.addEventListener('change',function(e){
            self.postMessage({
                type: 'time_change',
                value: this.value
            })
        })
    },
    createKitBox : function(parentObj){
        var select = document.createElement('select')
        parentObj.appendChild(select)
        var empty_option = document.createElement('option')
        empty_option.setAttribute('value', '')
        empty_option.innerHTML =''
        select.appendChild(empty_option)
        select.addEventListener('change',function(e){
            value = select.options[e.target.selectedIndex].value
            self.postMessage({
                type: 'complect_change',
                value: value
            })
        })
        var kits = document.getElementsByName('f22')[0].getElementsByTagName('small')[0].getElementsByTagName('a')
        for(var i=0; i<kits.length; i++){
            if(!kits[i].hasAttribute('onclick')){
                tomato.addSelectOptions(select, kits[i].href, kits[i].innerHTML)
            }

        }
    },
    createPanel: function(parentObj){
        tomato.createTimeBox(parentObj)
        tomato.createKitBox(parentObj)
        document.getElementsByName(tomato.url)[0].selected=true

    },
    addSelectOptions: function(select, value, name){
        var option = document.createElement('option')
        option.setAttribute('value', value)
        option.setAttribute('name', value)
        option.innerHTML = name
        select.appendChild(option)
    }
}
tomato.start();